package fr.benoit;

public class Main {

	public static void main(String[] args) {

		View vue = new View();
		vue.init();

		Model m = new Model();
		m.addObserver(vue.getLabel1());
		m.addObserver(vue.getLabel2());
		m.addObserver(vue.getLabel3());
		m.addObserver(vue.getLabel4());
		m.addObserver(vue.getLabel5());

		Controller c = new Controller( m, vue.getTextField() );

		// G�re l'action de click sur le bouton.
		vue.getButton().addActionListener(c);

	}

}
