package fr.benoit;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JTextField;

public class Controller implements ActionListener {

	private Model m;
	private JTextField textField;

	public Controller(Model m, JTextField textField) {
		super();
		this.m = m;
		this.textField = textField;
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		m.setText(this.textField.getText());
	}

	public Model getM() {
		return m;
	}

	public void setM(Model m) {
		this.m = m;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

}
