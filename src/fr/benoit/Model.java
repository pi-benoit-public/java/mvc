package fr.benoit;

import java.util.Observable;

public class Model extends Observable {

	String text;

	public Model() {
		super();
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
		this.setChanged();
		this.notifyObservers();
	}

}
