package fr.benoit;

import java.awt.Dimension;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

public class View {

	private JFrame frame;
	private JTextField textField;
	private JButton button;
	private JLabel label;

	private JLabelObserver label1, label2, label3, label4, label5;

	public View() {
		super();
	}

	public void init() {

		frame = new JFrame("Ma fen�tre");
		label = new JLabel("Ecris quelque chose");
		label1 = new JLabelObserver();
		label2 = new JLabelObserver();
		label3 = new JLabelObserver();
		label4 = new JLabelObserver();
		label5 = new JLabelObserver();
		textField = new JTextField();
		button = new JButton("OK");

		Dimension labelDimension = new Dimension(150,30);

		label.setPreferredSize(labelDimension);
		textField.setPreferredSize(new Dimension(100,30));
		button.setPreferredSize(new Dimension(80, 30));

		label1.setPreferredSize(labelDimension);
		label2.setPreferredSize(labelDimension);
		label3.setPreferredSize(labelDimension);
		label5.setPreferredSize(labelDimension);
		label5.setPreferredSize(labelDimension);


//		textField.setColumns(5);
		frame.setLayout(new FlowLayout());
//		frame.setLocation(GraphicsEnvironment.getLocalGraphicsEnvironment().getCenterPoint());
//		frame.setMinimumSize(null);
		frame.setVisible(true);

		frame.getContentPane().add(label);
		frame.getContentPane().add(textField);
		frame.getContentPane().add(button);

		frame.getContentPane().add(label1);
		frame.getContentPane().add(label2);
		frame.getContentPane().add(label3);
		frame.getContentPane().add(label4);
		frame.getContentPane().add(label5);

		frame.pack();
	}


//	public Vue() {
//		this.setVisible(true);
//		this.setSize(600, 400);
//		this.setLocationRelativeTo(null);
//		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
//		this.setLayout(new BorderLayout());
//		this.add(button,BorderLayout.NORTH);
//	}



	public JFrame getFrame() {
		return frame;
	}

	public void setFrame(JFrame frame) {
		this.frame = frame;
	}

	public JTextField getTextField() {
		return textField;
	}

	public void setTextField(JTextField textField) {
		this.textField = textField;
	}

	public JButton getButton() {
		return button;
	}

	public void setButton(JButton button) {
		this.button = button;
	}

	public JLabel getLabel() {
		return label;
	}

	public void setLabel(JLabel label) {
		this.label = label;
	}

	public JLabelObserver getLabel1() {
		return label1;
	}

	public void setLabel1(JLabelObserver label1) {
		this.label1 = label1;
	}

	public JLabelObserver getLabel2() {
		return label2;
	}

	public void setLabel2(JLabelObserver label2) {
		this.label2 = label2;
	}

	public JLabelObserver getLabel3() {
		return label3;
	}

	public void setLabel3(JLabelObserver label3) {
		this.label3 = label3;
	}

	public JLabelObserver getLabel4() {
		return label4;
	}

	public void setLabel4(JLabelObserver label4) {
		this.label4 = label4;
	}

	public JLabelObserver getLabel5() {
		return label5;
	}

	public void setLabel5(JLabelObserver label5) {
		this.label5 = label5;
	}

}
