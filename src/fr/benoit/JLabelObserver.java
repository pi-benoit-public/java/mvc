package fr.benoit;

import java.util.Observable;
import java.util.Observer;

import javax.swing.JLabel;

public class JLabelObserver extends JLabel implements Observer {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public JLabelObserver() {
		super();
	}

	@Override
	public void update(Observable o, Object arg) {
		Model m = (Model) o;
		this.setText(m.getText());
	}

}
